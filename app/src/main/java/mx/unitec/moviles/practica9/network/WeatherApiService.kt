package mx.unitec.moviles.practica9.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import mx.unitec.moviles.practica9.model.WeatherResponse
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_URL = "https://api.openweathermap.org/"
private const val APPID = "2e65127e909e178d0af311a81f39948c"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

  private val retrofit = Retrofit.Builder()
      //.addConverterFactory(ScalarsConverterFactory.create())
      .addConverterFactory(MoshiConverterFactory.create())
      .addCallAdapterFactory(CoroutineCallAdapterFactory())
      .baseUrl(BASE_URL)
      .build()

interface WeatherApiService {
    @GET("data/2.5/weather}?")
    fun getWeather(@Query("lat") lat:String, @Query("lon") long: String,
                   @Query("APPID") app_id: String = APPID) :
            //Deferred<String>
            Deferred<WeatherResponse>

}

object WeatherApi {
    val retrofitService : WeatherApiService by lazy{
        retrofit.create(WeatherApiService::class.java)
    }
}



