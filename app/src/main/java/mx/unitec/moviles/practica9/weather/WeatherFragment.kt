package mx.unitec.moviles.practica9.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import mx.unitec.moviles.practica9.R
import mx.unitec.moviles.practica9.databinding.WeatherFragmentBinding
import mx.unitec.moviles.practica9.model.Coordenada

class WeatherFragment : Fragment() {

    companion object {
        fun newInstance() = WeatherFragment()
    }

    private val viewModel: WeatherViewModel by lazy {
        ViewModelProviders.of(this).get(WeatherViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = WeatherFragmentBinding.inflate(inflater)

        binding.setLifecycleOwner (this)
        binding.viewModel = viewModel
        return binding.root
        }
     public fun updateLocation(coordenada: Coordenada){
         viewModel.coordenada = coordenada
         viewModel.getWeatherProperties()
     }

}